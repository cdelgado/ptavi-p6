#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Recibe mensajes y responde a los clientes
        for line in self.rfile:
            # Leyendo línea a línea lo que nos envía el cliente
            text = line.decode('utf-8')
            method = text.split(' ')[0]
            print("El cliente nos manda " + text)
            if method == 'INVITE':
                self.wfile.write(b"SIP/2.0 100 Trying \r\n\r\n"
                                 b"SIP/2.0 180 Ringing \r\n\r\n"
                                 b"SIP/2.0 200 OK\r\n\r\n")
                self.wfile.write(b"content_length=")
                description = "v=0\r\n" + "o==robing@gotham.com 127.0.0.1\r\n" + \
                              "s=missesion\r\n" + "t=0\r\n" + \
                              "m=audio 67876 RTP\r\n"
                length = str(len(bytes(description, 'utf-8'))) + "\r\n\r\n"
                self.wfile.write(bytes(length, 'utf-8'))
                self.wfile.write(bytes(description, 'utf-8'))

            elif method == 'ACK':
                send_audio(text)
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif method == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")


def send_audio(text):
    aleat = random.randint(0, 100000)
    RTP_header = simplertp.RtpHeader()
    RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, marker=0, ssrc=aleat)
    audio = simplertp.RtpPayloadMp3(sys.argv[3])
    simplertp.send_rtp_packet(RTP_header, audio, "127.0.0.1", 23032)


if __name__ == "__main__":
    if len(sys.argv) != 4:
        sys.exit('Usage: python3 server.py IP port audio_file')
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    AUDIO_FILE = sys.argv[3]
    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((IP, PORT), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit("Finalizado servidor")
