#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

try:
    if len(sys.argv) != 3:
        sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
    else:
        RECEIVER = SERVER[0]
        METHOD = sys.argv[1]
        SERVER = sys.argv[2].split("@")
        IP = SERVER[1].split(":")[0]
        PORT = int(SERVER[1].split(":")[1])
except ValueError:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, PORT))
    message = METHOD + " sip:" + RECEIVER + "@" + IP + " SIP/2.0\r\n\r\n"
    my_socket.send(bytes(message, 'utf-8'))
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))
    if data.decode('utf-8').startswith("SIP/2.0 100 "
                                       "Trying\r\n\r\nSIP/2.0 180 ringing"
                                       "\r\n\r\nSIP/2.0 200 OK\r\n"):
        message = "ACK sip:" + RECEIVER + "@" + IP + " SIP/2.0\r\n\r\n"
        print("Enviando: " + message)
        my_socket.send(bytes(message, 'utf-8'))
    print("Terminando socket...")

print("Fin.")